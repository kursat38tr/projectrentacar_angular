import { CarCategory } from './car-category';

export class Car {
  id: number
  name: string;
  description: string;
  licensePlate: string;
  fuel: string;
  currentPrice: number;
  availability: boolean;
  imageUrl: string;
  category: CarCategory;
}
