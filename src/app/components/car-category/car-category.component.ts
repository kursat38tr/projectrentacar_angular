import { HttpErrorResponse } from '@angular/common/http';
import { Component, OnInit } from '@angular/core';
import { Subscription } from 'rxjs';
import { NotificationType } from 'src/app/enum/notification-type.enum';
import { CarCategory } from 'src/app/model/car-category';
import { CarService } from 'src/app/services/car.service';
import { NotificationService } from 'src/app/services/notification.service';

@Component({
  selector: 'app-car-category',
  templateUrl: './car-category.component.html',
  styleUrls: ['./car-category.component.css']
})
export class CarCategoryComponent implements OnInit {

  public carCategory: CarCategory[];
  private subscriptions: Subscription[] = [];

  constructor(private carCategoryService: CarService, private notificationService: NotificationService) { }

      ngOnInit(): void {
        this.getCarCategory();

      }


      public getCarCategory(): void {
        
        this.carCategoryService.getCarsCategory().subscribe(
          data => {
            console.log('Categories' + JSON.stringify(data));
            this.carCategory = data;
          }
        );
      }
  }


