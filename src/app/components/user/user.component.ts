import {
  HttpErrorResponse,
  HttpEvent,
  HttpEventType,
} from '@angular/common/http';
import {Component, OnInit} from '@angular/core';
import {FormControl, FormGroup, NgForm} from '@angular/forms';
import {ActivatedRoute, Router} from '@angular/router';
import {emit} from 'process';
import {BehaviorSubject, Subscription} from 'rxjs';
import {NotificationType} from 'src/app/enum/notification-type.enum';
import {Role} from 'src/app/enum/role.enum';
import {CustomHttpResponse} from '../../model/custom-http-response';
import {FileUploadStatus} from 'src/app/model/file-upload.status';
import {User} from 'src/app/model/user';
import {AuthenticationService} from 'src/app/services/authentication.service';
import {NotificationService} from 'src/app/services/notification.service';
import {UserService} from 'src/app/services/user.service';
import {CarService} from 'src/app/services/car.service';
import {Car} from 'src/app/model/car';
import {CarCategory} from 'src/app/model/car-category';

@Component({
  selector: 'app-user',
  templateUrl: './user.component.html',
  styleUrls: ['./user.component.css'],
})
export class UserComponent implements OnInit {
  private titleSubject = new BehaviorSubject<string>('Users');
  public titleAction$ = this.titleSubject.asObservable();
  public users: User[];
  public user: User;
  public refreshing: boolean;
  public selectedUser: User;
  public selectedCar: Car;
  public fileName: string;
  public profileImage: File;
  private subscriptions: Subscription[] = [];
  public editUser = new User();
  private currentUsername: string;
  public fileStatus = new FileUploadStatus();
  public cars: Car[] = [];
  public carCategories: CarCategory[];
  public car = new Car();
  public selectedCategory: string = "";

  public carData = new FormGroup({
    licensePlate: new FormControl(''),
    name: new FormControl(''),
    description: new FormControl(''),
    fuel: new FormControl(''),
    currentPrice: new FormControl(''),
    availability: new FormControl(''),
    category: new FormControl('')
  })


  constructor(
    private router: Router,
    private authenticationService: AuthenticationService,
    private userService: UserService,
    private notificationService: NotificationService,
    private carService: CarService,
    private route: ActivatedRoute
  ) {
  }

  ngOnInit(): void {
    this.user = this.authenticationService.getUserFromLocalCache();
    this.getUsers(true);
    this.getCars(true);
    this.getCarCategory(true);
    // this.listProductCategories();
  }

  public changeTitle(title: string): void {
    this.titleSubject.next(title);
  }


  public handleCarDetails() {
      const theCarId: number = +this.route.snapshot.paramMap.get('id');
      this.carService.getCar(theCarId).subscribe(data=> {
        this.car = data;
        console.log("jemoder " + data);
      })
  }



  public getCars(showNotification: boolean): void {
    this.subscriptions.push(
      this.carService.getOnlyCars().subscribe(
        (data: Car[]) => {
          this.cars = data as Car[];
          console.log(this.cars);
          if (showNotification) {
            this.sendNotification(
              NotificationType.SUCCESS,
              ` Cars loaded successfully.`
            );
          }
        },
        (errorResponse: HttpErrorResponse) => {
          this.sendNotification(
            NotificationType.ERROR,
            errorResponse.error.message
          );
        }
      )
    );
  }

  // listProductCategories() {

  //   this.carService.getOnlyCarsCategory().subscribe(
  //     (data : CarCategory[]) => {
  //       console.log('Product Categories=' + JSON.stringify(data));
  //       this.carCategories = data;
  //     }
  //   );
  // }

  public getCarCategory(showNotification: boolean): void {
    this.subscriptions.push(
      this.carService.getOnlyCarsCategoryId().subscribe(
        (data: CarCategory[]) => {
          this.carCategories = data;
          console.log(JSON.stringify(this.carCategories));
          if (showNotification) {
            this.sendNotification(
              NotificationType.SUCCESS,
              ` Cars loaded successfully.`
            );
          }
        },
        (errorResponse: HttpErrorResponse) => {
          this.sendNotification(
            NotificationType.ERROR,
            errorResponse.error.message
          );
        }
      )
    );
  }

  public getUsers(showNotification: boolean): void {
    this.refreshing = true;
    this.subscriptions.push(
      this.userService.getUsers().subscribe(
        (response: User[]) => {
          this.userService.addUsersToLocalCache(response);
          this.users = response;
          this.refreshing = false;
          if (showNotification) {
            this.sendNotification(
              NotificationType.SUCCESS,
              `${response.length} user(s) loaded successfully.`
            );
          }
        },
        (errorResponse: HttpErrorResponse) => {
          this.sendNotification(
            NotificationType.ERROR,
            errorResponse.error.message
          );
          this.refreshing = false;
        }
      )
    );
  }

  public onSelectCar(selectedCar: Car): void {
    this.selectedCar = selectedCar;
    this.clickButton('openCarInfo');
  }

  public onSelectUser(selectedUser: User): void {
    this.selectedUser = selectedUser;
    this.clickButton('openCarInfo');
  }

  public onProfileImageChange(fileName: string, profileImage: File): void {
    this.fileName = fileName;
    this.profileImage = profileImage;
  }

  public saveNewUser(): void {
    this.clickButton('new-user-save');
  }

  public saveNewCar(): void {
    this.clickButton('new-car-save');
  }

  // ADD NEW CAR



  // public onAddNewCar(carForm: NgForm): void {
  //   console.log(carForm.value);
  //   const formData = this.carService.createCarFormDate(carForm.value);
  //
  //   this.subscriptions.push(
  //     this.carService.addCar(carForm).subscribe(
  //       (response: Car) => {
  //         this.clickButton('new-user-close');
  //         this.getCars(false);
  //         console.log(JSON.stringify(carForm));
  //         carForm.reset();
  //         this.sendNotification(
  //           NotificationType.SUCCESS,
  //           ` added successfully`
  //         );
  //       },
  //     )
  //   );
  // }

  public onAddNewUser(userForm: NgForm): void {
    const formData = this.userService.createUserFormDate(
      null,
      userForm.value,
      this.profileImage
    );
    this.subscriptions.push(
      this.userService.addUser(formData).subscribe(
        (response: User) => {
          this.clickButton('new-user-close');
          this.getUsers(false);
          this.fileName = null;
          this.profileImage = null;
          userForm.reset();
          this.sendNotification(
            NotificationType.SUCCESS,
            `${response.firstName} ${response.lastName} added successfully`
          );
        },
        (errorResponse: HttpErrorResponse) => {
          this.sendNotification(
            NotificationType.ERROR,
            errorResponse.error.message
          );
          this.profileImage = null;
        }
      )
    );
  }

  public onUpdateUser(): void {
    const formData = this.userService.createUserFormDate(
      this.currentUsername,
      this.editUser,
      this.profileImage
    );
    this.subscriptions.push(
      this.userService.updateUser(formData).subscribe(
        (response: User) => {
          this.clickButton('closeEditUserModalButton');
          this.getUsers(false);
          this.fileName = null;
          this.profileImage = null;
          this.sendNotification(
            NotificationType.SUCCESS,
            `${response.firstName} ${response.lastName} updated successfully`
          );
        },
        (errorResponse: HttpErrorResponse) => {
          this.sendNotification(
            NotificationType.ERROR,
            errorResponse.error.message
          );
          this.profileImage = null;
        }
      )
    );
  }

  public onUpdateCurrentUser(user: User): void {
    this.refreshing = true;
    this.currentUsername = this.authenticationService.getUserFromLocalCache().username;
    const formData = this.userService.createUserFormDate(
      this.currentUsername,
      user,
      this.profileImage
    );
    this.subscriptions.push(
      this.userService.updateUser(formData).subscribe(
        (response: User) => {
          this.authenticationService.addUserToLocalCache(response);
          this.getUsers(false);
          this.fileName = null;
          this.profileImage = null;
          this.sendNotification(
            NotificationType.SUCCESS,
            `${response.firstName} ${response.lastName} updated successfully`
          );
        },
        (errorResponse: HttpErrorResponse) => {
          this.sendNotification(
            NotificationType.ERROR,
            errorResponse.error.message
          );
          this.refreshing = false;
          this.profileImage = null;
        }
      )
    );
  }

  public onUpdateProfileImage(): void {
    const formData = new FormData();
    formData.append('username', this.user.username);
    formData.append('profileImage', this.profileImage);
    this.subscriptions.push(
      this.userService.updateProfileImage(formData).subscribe(
        (event: HttpEvent<any>) => {
          this.reportUploadProgress(event);
        },
        (errorResponse: HttpErrorResponse) => {
          this.sendNotification(
            NotificationType.ERROR,
            errorResponse.error.message
          );
          this.fileStatus.status = 'done';
        }
      )
    );
  }

  private reportUploadProgress(event: HttpEvent<any>): void {
    switch (event.type) {
      case HttpEventType.UploadProgress:
        this.fileStatus.percentage = Math.round(
          (100 * event.loaded) / event.total
        );
        this.fileStatus.status = 'progress';
        break;
      case HttpEventType.Response:
        if (event.status === 200) {
          this.user.profileImageUrl = `${
            event.body.profileImageUrl
          }?time=${new Date().getTime()}`;
          this.sendNotification(
            NotificationType.SUCCESS,
            `${event.body.firstName}\'s profile image updated successfully`
          );
          this.fileStatus.status = 'done';
          break;
        } else {
          this.sendNotification(
            NotificationType.ERROR,
            `Unable to upload image. Please try again`
          );
          break;
        }
      default:
        `Finished all processes`;
    }
  }

  public updateProfileImage(): void {
    this.clickButton('profile-image-input');
  }

  public onLogOut(): void {
    this.authenticationService.logOut();
    this.router.navigate(['/login']);
    this.sendNotification(
      NotificationType.SUCCESS,
      `You've been successfully logged out`
    );
  }

  public onResetPassword(emailForm: NgForm): void {
    this.refreshing = true;
    const emailAddress = emailForm.value['reset-password-email'];
    this.subscriptions.push(
      this.userService.resetPassword(emailAddress).subscribe(
        (response: CustomHttpResponse) => {
          this.sendNotification(NotificationType.SUCCESS, response.message);
          this.refreshing = false;
        },
        (error: HttpErrorResponse) => {
          this.sendNotification(NotificationType.WARNING, error.error.message);
          this.refreshing = false;
        },
        () => emailForm.reset()
      )
    );
  }

  public onDeleteUder(username: string): void {
    this.subscriptions.push(
      this.userService.deleteUser(username).subscribe(
        (response: CustomHttpResponse) => {
          this.sendNotification(NotificationType.SUCCESS, response.message);
          this.getUsers(false);
        },
        (error: HttpErrorResponse) => {
          this.sendNotification(NotificationType.ERROR, error.error.message);
        }
      )
    );
  }

  public onEditUser(editUser: User): void {
    this.editUser = editUser;
    this.currentUsername = editUser.username;
    this.clickButton('openUserEdit');
  }

  public searchUsers(searchTerm: string): void {
    const results: User[] = [];
    for (const user of this.userService.getUsersFromLocalCache()) {
      if (
        user.firstName.toLowerCase().indexOf(searchTerm.toLowerCase()) !== -1 ||
        user.lastName.toLowerCase().indexOf(searchTerm.toLowerCase()) !== -1 ||
        user.username.toLowerCase().indexOf(searchTerm.toLowerCase()) !== -1 ||
        user.userId.toLowerCase().indexOf(searchTerm.toLowerCase()) !== -1
      ) {
        results.push(user);
      }
    }
    this.users = results;
    if (results.length === 0 || !searchTerm) {
      this.users = this.userService.getUsersFromLocalCache();
    }
  }

  public get isAdmin(): boolean {
    return (
      this.getUserRole() === Role.ADMIN ||
      this.getUserRole() === Role.SUPER_ADMIN
    );
  }

  public get isManager(): boolean {
    return this.isAdmin || this.getUserRole() === Role.MANAGER;
  }

  public get isAdminOrManager(): boolean {
    return this.isAdmin || this.isManager;
  }

  private getUserRole(): string {
    return this.authenticationService.getUserFromLocalCache().role;
  }

  private sendNotification(
    notificationType: NotificationType,
    message: string
  ): void {
    if (message) {
      this.notificationService.notify(notificationType, message);
    } else {
      this.notificationService.notify(
        notificationType,
        'An error occurred. Please try again.'
      );
    }
  }

  private clickButton(buttonId: string): void {
    document.getElementById(buttonId).click();
  }

  ngOnDestroy(): void {
    this.subscriptions.forEach((sub) => sub.unsubscribe());
  }


  public createCar(carData: FormGroup) {
    console.log(carData.value);

    const formData = this.carService.createCarFormDate(carData.value);


     let JSON_CarData = JSON.stringify(this.carData.value);

     console.log("hallo" + JSON_CarData);


    this.carService.addCar(formData).subscribe(
      (response: Car) => {
        this.clickButton('new-car-close');
        this.fileName = null;
        this.profileImage = null;
        carData.reset();
        this.sendNotification(
          NotificationType.SUCCESS, ""
        );
      },
    )


  }

  onAddNewCar(newCarForm: NgForm) {

  }
}
