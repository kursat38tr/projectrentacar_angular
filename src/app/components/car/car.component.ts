import { HttpErrorResponse } from '@angular/common/http';
import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Subscription } from 'rxjs';
import { CartItem } from 'src/app/common/cart-item';
import { NotificationType } from 'src/app/enum/notification-type.enum';
import { Car } from 'src/app/model/car';
import { CarService } from 'src/app/services/car.service';
import { CartService } from 'src/app/services/cart.service';
import { NotificationService } from 'src/app/services/notification.service';

@Component({
  selector: 'app-car',
  templateUrl: './car.component.html',
  styleUrls: ['./car.component.css']
})
export class CarComponent implements OnInit {

  public currentCategoryId: number = 1;
  public cars: Car[];
  public previousCategoryId: number = 1;
  private subscriptions: Subscription[] = [];
  public searchMode: boolean = false;


  public thePageNumber: number = 1;
  public thePageSize: number = 5;
  public theTotalElements: number = 0;

  public previousKeyword: string = null;
  

  

  constructor(private carService: CarService, private notificationService: NotificationService, private route: ActivatedRoute, private cartService: CartService,) { }

  ngOnInit(): void {

    this.route.paramMap.subscribe(() =>{
    // this.getCarsList();
    //  this.getUsers(true);
      this.getCars(true);
      
    });
  }

  public getCars(showNotification: boolean): void {

    this.searchMode = this.route.snapshot.paramMap.has('keyword');

    if (this.searchMode) {
      this.handleSearchCars();
    } else {
      this.handleListCars();
    }

    // this.subscriptions.push(
    //   this.carService.getCarList(this.currentCategoryId).subscribe(
    //      (data : Car[]) => {
    //       console.log(data);
    //       this.cars = data as Car[];
    //       console.log(this.cars)
    //       if (showNotification) {
    //         this.sendNotification(
    //           NotificationType.SUCCESS,
    //           ` Cars loaded successfully.`
    //         );
    //       }
    //     },
    //     (errorResponse: HttpErrorResponse) => {
    //       this.sendNotification(
    //         NotificationType.ERROR,
    //         errorResponse.error.message
    //       );
    //     }
    //   )
    // );
  }



  public handleSearchCars() {
    const theKeyword: string = this.route.snapshot.paramMap.get('keyword');


     // now search for the products using keyword
     this.carService.searchCars(theKeyword).subscribe(
      data => {
        this.cars = data;
      }
    )
    // if we have a different keyword than previous
    // then set thePageNumber to 1

    // if (this.previousKeyword != theKeyword) {
    //   this.thePageNumber = 1;
    // }

    // this.previousKeyword = theKeyword;

    // console.log(`keyword=${theKeyword}, thePageNumber=${this.thePageNumber}`);

    // now search for the products using keyword
    // this.carService
    //   .searchCarsPaginate (
    //     this.thePageNumber - 1,
    //     this.thePageSize,
    //     theKeyword
    //   )
    //   .subscribe(this.processResult());
  }


   // public getCarListPaginate(thePage: number, thePageSize : number, theCategoryId: number, )

  public handleListCars() {

    const hasCategoryId: boolean = this.route.snapshot.paramMap.has('id');

    console.log(hasCategoryId);

    if (hasCategoryId) {
      // get the "id" param string. convert string to a number using the "+" symbol
      this.currentCategoryId = +this.route.snapshot.paramMap.get('id');
    } else {
      // not category id available ... default to category id 1
      this.currentCategoryId = 1;

    }

    console.log(this.currentCategoryId);
    
    
      // // if we have a different category id than previous
    // // then set thePageNumber back to 1
    if (this.previousCategoryId != this.currentCategoryId) {
      this.thePageNumber = 1;
    }

    this.previousCategoryId = this.currentCategoryId;

    console.log(
      `currentCategoryId=${this.currentCategoryId}, thePageNumber=${this.thePageNumber}`
    );


    this.carService
      .getCarListPaginate(
        this.thePageNumber - 1,
        this.thePageSize,
        this.currentCategoryId
      )
      .subscribe(this.processResult());

    this.carService.getCarList(this.currentCategoryId).subscribe(data => {
        this.cars = data;
    })
  }



  public processResult() {
    return (data) => {
      this.cars = data._embedded.products;
      this.thePageNumber = data.page.number + 1;
      this.thePageSize = data.page.size;
      this.theTotalElements = data.page.totalElements;
    };

    
  }

  updatePageSize(pageSize: number) {
    this.thePageSize = pageSize;
    this.thePageNumber = 1;
    this.getCars(true);
  }

   addToCart(theCar: Car) {
    console.log(`Adding to cart: ${theCar.name}, ${theCar.currentPrice}`);

    // TODO ... do the real work
    const theCartItem = new CartItem(theCar);

    this.cartService.addToCart(theCartItem);
  }


    // // check if "id" parameter is available
    // const hasCategoryId: boolean = this.route.snapshot.paramMap.has('id');

    // console.log(hasCategoryId);

    // if (hasCategoryId) {
    //   // get the "id" param string. convert string to a number using the "+" symbol
    //   this.currentCategoryId = +this.route.snapshot.paramMap.get('id');
    // } else {
    //   // not category id available ... default to category id 1
    //   this.currentCategoryId = 1;
    // }

    // //
    // // Check if we have a different category than previous
    // // Note: Angular will reuse a component if it is currently being viewed
    // //

    // // if we have a different category id than previous
    // // then set thePageNumber back to 1
    // if (this.previousCategoryId != this.currentCategoryId) {
    //   this.thePageNumber = 1;
    // }

    // this.previousCategoryId = this.currentCategoryId;

    // console.log(
    //   `currentCategoryId=${this.currentCategoryId}, thePageNumber=${this.thePageNumber}`
    // );

    // // now get the products for the given category id
    // this.productService
    //   .getProductListPaginate(
    //     this.thePageNumber - 1,
    //     this.thePageSize,
    //     this.currentCategoryId
    //   )
    //   .subscribe(this.processResult());
  

  private sendNotification(
    notificationType: NotificationType,
    message: string
  ): void {
    if (message) {
      this.notificationService.notify(notificationType, message);
    } else {
      this.notificationService.notify(
        notificationType,
        'An error occurred. Please try again.'
      );
    }
  }

}
