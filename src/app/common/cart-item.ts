import { Car } from '../model/car';

export class CartItem {
  id: number;
  name: string;
  imageUrl: string;
  currentPrice: number;
  quantity: number;

  constructor(car: Car) {
    this.id = car.id;
    this.name = car.name;
    this.imageUrl = car.imageUrl;
    this.currentPrice = car.currentPrice;

    this.quantity = 1;
  }
}
