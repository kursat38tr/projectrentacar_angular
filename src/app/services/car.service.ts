import {Injectable} from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {Observable} from 'rxjs';
import {map} from 'rxjs/operators';
import {Car} from '../model/car';
import {TestBed} from '@angular/core/testing';
import {CarCategory} from '../model/car-category';
import {FormGroup, NgForm} from "@angular/forms";

@Injectable({
  providedIn: 'root',
})
export class CarService {
  private rootUrl = 'http://localhost:8080';
  private categoriesUrl = 'http://localhost:8080/car-category';
  private baseurl = 'http://localhost:8080/cars';

  private categoryUrl = 'http://localhost:8080/car-category';

  private carCrl = 'http://localhost:8080';

  private url = 'http://localhost:8080/car';

  public carCategory = new CarCategory();

  // private categoryUrl = 'http://localhost:8080/cars';

  constructor(private httpClient: HttpClient) {
  }

  public getCarList(categoryId: number): Observable<Car[]> {
    const categoryUrl = `${this.baseurl}/search/findByCategoryId?id=${categoryId}`;

    return this.httpClient
      .get<GetResponseCar>(categoryUrl)
      .pipe(map((response) => response._embedded.cars));
  }


  public getCar(theCarId: number): Observable<Car> {

    const productUrl = `${this.baseurl}/${theCarId}`;

    return this.httpClient.get<Car>(productUrl);
  }


  private getCars(searchUrl: string): Observable<Car[]> {
    return this.httpClient.get<GetResponseCar>(searchUrl).pipe(map(response => response._embedded.cars));
  }

  public getOnlyCars(): Observable<Car[]> {
    const categoryUrl = `${this.baseurl}`;

    return this.httpClient
      .get<GetResponseCar>(categoryUrl)
      .pipe(map((response) => response._embedded.cars));
  }

  // public getCarCategories(): Observable<CarCategory[]> {

  //   var test = this.httpClient.get<GetResponseCarCategory>(`${this.rootUrl}/car-category`).pipe(
  //     map(response => response._embedded.carsCategory)
  //   );

  //   console.log();
  //   return test;
  // }

  // getProductCategories(): Observable<CarCategory[]> {

  //   var test =  this.httpClient.get<GetResponseCarCategory>('http://localhost:8080/car-category').pipe(
  //     map(response => response._embedded.carsCategory)
  //   );

  //   console.log(test);

  //   return
  // }

  // public getOnlyCarsCategory(): Observable<CarCategory[]> {
  //    var test = this.httpClient.get<GetResponseCarCategory>('http://localhost:8080/car-category').subscribe( data => {
  //      this.carCategories = data
  //    }
  //   );

  //   console.log(test);
  //   return test;
  // }


  // getProductCategories(): Observable<CarCategory[]> {
  //   return this.httpClient
  //     .get<GetResponseCarCategory>(this.categoryUrl)
  //     .pipe(map((response) => response._embedded.carCategory));
  // }

  public getOnlyCarsCategoryId(): Observable<CarCategory[]> {
    return this.httpClient.get<CarCategory[]>(
      `${this.rootUrl}/carCategory/list`
    );
  }


  public getCarsCategory(): Observable<CarCategory[]> {
    return this.httpClient.get<GetResponseCarCategory>(this.categoryUrl).pipe(map(response => response._embedded.carCategory));
  }

  public addCar(formData: FormData): Observable<Car> {
    var test = this.httpClient.post<Car>(`${this.carCrl}/car/createCar`, formData);

    console.log(formData);

    return test;
  }


  public searchCars(theKeyword: string): Observable<Car[]> {
    const searchUrl = `${this.baseurl}/search/findByNameContaining?name=${theKeyword}`;

    return this.getCars(searchUrl);
  }

  public getCarListPaginate(
    thePage: number,
    thePageSize: number,
    theCategoryId: number
  ): Observable<GetResponseCar> {
    // need to build URL based on category id, page and size

    const searchUrl =
      `${this.baseurl}/search/findByCategoryId?id=${theCategoryId}` +
      `&page=${thePage}&size=${thePageSize}`;

    return this.httpClient.get<GetResponseCar>(searchUrl);
  }


  public createCarFormDate(
    car: Car
    // profileImage: File
  ): FormData {
    const formData = new FormData();
    formData.append('licensePlate', car.licensePlate);
    formData.append('name', car.name);
    formData.append('description', car.description);
    formData.append('fuel', car.fuel);
    formData.append('currentPrice', JSON.stringify(car.currentPrice));
    console.log("HJemoedmkjdf" +formData.append('category', JSON.stringify(this.carCategory.id)));

    formData.append('availability', JSON.stringify(car.availability));

    console.log(formData);

    return formData;
  }
}

interface GetResponseCar {
  _embedded: {
    cars: Car[];
  };

  page: {
    size: number;
    totalElements: number;
    totalPages: number;
    number: number;
  };
}

interface GetResponseCarCategory {
  _embedded: {
    carCategory: CarCategory[];
  };
  "_links":{
    "href" : string;
  }
}

