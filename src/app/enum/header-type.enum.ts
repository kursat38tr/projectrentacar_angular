export enum HeaderType {
  AUTORIZATION = 'Authorization',
  JWT_TOKEN = 'Jwt-Token',
}
